package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.service.IAuthService;
import ru.t1.semikolenov.tm.api.service.IPropertyService;
import ru.t1.semikolenov.tm.api.service.IUserService;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.exception.entity.UserNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyLoginException;
import ru.t1.semikolenov.tm.exception.field.EmptyPasswordException;
import ru.t1.semikolenov.tm.exception.user.AccessDeniedException;
import ru.t1.semikolenov.tm.exception.user.AuthenticationException;
import ru.t1.semikolenov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.semikolenov.tm.exception.user.PermissionException;
import ru.t1.semikolenov.tm.model.User;
import ru.t1.semikolenov.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    public void login(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @Nullable final String hash = HashUtil.salt(password, secret, iteration);
        if (hash == null) throw new IncorrectLoginOrPasswordException();
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @NotNull
    @Override
    public User registry(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth() || userId == null) throw new AccessDeniedException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void checkRoles(final @Nullable Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
